#! /usr/bin/env bash 

domain=${1}

c=$(curl -s ${domain} | atr src | grep -i "js" )

#echo ${c}

for i in $( echo "${c}")
do
	echo ${i}
	filename=$(echo ${i} | sed 's/^.*\///')
	
	curl  "${i}" | js-beautify  1> ${filename}
done
